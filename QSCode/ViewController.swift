//
//  ViewController.swift
//  QRReaderDemo
//
//  Created by Simon Ng on 23/11/14.
//  Copyright (c) 2014 AppCoda. All rights reserved.
//

import UIKit
import AVFoundation
import CoreData
import Compression
import Foundation
import ImageIO

class ViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var backgroundView: UIView!
    
    let imagePicker = UIImagePickerController()
    
    weak var parentPageViewController: PageViewController!
    
    func sendFilesChosen(sender: AnyObject) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    
    // MARK: - UIImagePicker Methods
    
    var qrcodes = Array<UIImage>()
    var counter = 0
    var timer = NSTimer()
    
    func compressImage(img: UIImage) -> NSData{
        var actualHeight:Float = Float(img.size.height);
        var actualWidth:Float = Float(img.size.width);
        let maxHeight:Float = 150.0;
        let maxWidth:Float = 300;
        var imgRatio:Float = actualWidth/actualHeight;
        let maxRatio:Float = maxWidth/maxHeight;
        let compressionQuality:CGFloat = 0.5;//50 percent compression
        if (actualHeight > maxHeight || actualWidth > maxWidth){
            if(imgRatio < maxRatio){
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight;
                actualWidth = imgRatio * actualWidth;
                actualHeight = maxHeight;
            }
            else if(imgRatio > maxRatio){
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth;
                actualHeight = imgRatio * actualHeight;
                actualWidth = maxWidth;
            }
            else{
                actualHeight = maxHeight;
                actualWidth = maxWidth;
            }
        }
        
        let rect:CGRect = CGRectMake(0.0, 0.0, CGFloat(actualWidth), CGFloat(actualHeight));
        UIGraphicsBeginImageContext(rect.size);
        img.drawInRect(rect)
        let newimg:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        let imageData:NSData = UIImageJPEGRepresentation(newimg, compressionQuality)!;
        UIGraphicsEndImageContext();
        
        return imageData
    }
    
    func createThumbnailImage(data: NSData, imageSize:Int) -> CGImageRef! {
        var thumbnailSize: CFNumberRef = 0
        
        // Create an image source from NSData; no options.
        if let imgSource = CGImageSourceCreateWithData(data, nil) {
            
            // Set up the thumbnail options.
            let options = [
                kCGImageSourceCreateThumbnailWithTransform as NSString : kCFBooleanTrue,
                kCGImageSourceCreateThumbnailFromImageIfAbsent as NSString : kCFBooleanTrue,
                kCGImageSourceThumbnailMaxPixelSize as NSString : imageSize
            ]
            
            // Create the thumbnail image using the specified options.
            if let image = CGImageSourceCreateThumbnailAtIndex(imgSource, 0, options) {
                return image
            } else {
                print("Thumbnail image not created from image source.")
            }
        } else {
            print("Image source is NULL.")
        }
        
        return nil
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            /*let imageData1:NSData = UIImageJPEGRepresentation(pickedImage, 1.0)!;
             print(imageData1.length)
             let base64String1 = imageData1.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
             print(base64String1.characters.count)*/
            let imageData = compressImage(pickedImage)

            //let imageData:NSData = UIImageJPEGRepresentation(pickedImage, 1.0)!
            //let img:CGImage = self.createThumbnailImage(imageData, imageSize: 128)
            //var imageui: UIImage = UIImage(CGImage: img)
            //var imageData1 = UIImageJPEGRepresentation(imageui, 1)
            let base64String = imageData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))

            
            
            var chunks = base64String.split(1200)
            for i in 0 ..< chunks.count {
                let img: UIImage = generateQRCodeFromString("0||1||\(i)||\(chunks.count)||\(chunks[i])")!
                //var imageData = UIImagePNGRepresentation(pickedImage)
                //let base64String = imageData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
                qrcodes.append(img)
            }
            
            
            
        }
        
        dismissViewControllerAnimated(true, completion: nil)
        showQRCodeImages()
    }
    
    func doAnimation() {
        if counter == qrcodes.count - 1 {
            counter = 0
        } else {
            counter += 1
        }
        largeImageView.image = qrcodes[counter]
    }
    
    var animate: Bool = false {
        didSet {
            if animate == true {
                timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("doAnimation"), userInfo: nil, repeats: true)
            } else {
                timer.invalidate()
            }
        }
    }
    func showQRCodeImages() {
        backgroundView.hidden = false
        largeImageView.hidden = false
        cancelSendBtn.hidden = false
        view.bringSubviewToFront(backgroundView)
        view.bringSubviewToFront(largeImageView)
        
        animate = !animate
    }
    
    @IBOutlet weak var cancelSendBtn: UIButton!
    @IBAction func cancelSend(sender: AnyObject) {
        
        backgroundView.hidden = false
        largeImageView.image = nil
        largeImageView.hidden = true
        cancelSendBtn.hidden = true
        view.sendSubviewToBack(backgroundView)
        view.sendSubviewToBack(largeImageView)
    }
    
    
    func generateQRCodeFromString(string: String) -> UIImage? {
        let data = string.dataUsingEncoding(NSISOLatin1StringEncoding)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            filter.setValue("H", forKey: "inputCorrectionLevel")
            let transform = CGAffineTransformMakeScale(10, 10)
            
            if let output = filter.outputImage?.imageByApplyingTransform(transform) {
                return UIImage(CIImage: output)
            }
        }
        
        return nil
    }
    
    
    var appDel: AppDelegate!
    
    var context: NSManagedObjectContext!
    
    @IBOutlet weak var messageLabel:UILabel!
    @IBOutlet weak var largeImageView: UIImageView!
    var progress: KDCircularProgress!
    var progressLabel: UILabel!
    var numberLabel: UILabel!
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    var scanned:String?
    var numberOfImages:Int!
    
    var dict1 = [Int:String]()
    var dict1Count = [String:String]()
    var dict1Flag: Bool? // flag to signal first loop through image, so we can populate dict1Count only once
    var dict1Stop: Bool? // flag to know when to stop accounting for image 1
    var dict1Done: Bool?
    
    var dict2 = [Int:String]()
    var dict2Count = [String:String]()
    var dict2Flag: Bool? // flag to signal first loop through image, so we can populate dict1Count only once
    var dict2Stop: Bool? // flag to know when to stop accounting for image 1
    var dict2Done: Bool?
    
    
    var currentCount: Int!
    var maxCount: Int!
    
    // Added to support different barcodes
    let supportedBarCodes = [AVMetadataObjectTypeQRCode]
    //let supportedBarCodes = [AVMetadataObjectTypeQRCode, AVMetadataObjectTypeCode128Code, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeUPCECode, AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeAztecCode, AVMetadataObjectTypeEAN8Code]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        backgroundView.hidden = true
        cancelSendBtn.hidden = true
        
        appDel = UIApplication.sharedApplication().delegate as! AppDelegate
        context = appDel.managedObjectContext
        
        var scanRequest = NSFetchRequest(entityName: "Scans")
        
        scanRequest.returnsObjectsAsFaults = false
        
        var scanResults = try? context.executeFetchRequest(scanRequest)
        
        var errorPointer: NSError? = nil
        if scanResults?.count > 0 {
            for result: AnyObject in scanResults! {
                if let title = result.valueForKey("title") as? String {
                    if (title == "New Scan 1") || (title == "New Scan 2") {
                        context.deleteObject(result as! NSManagedObject)
                    }
                }
                
            }
        }
        
        let map = UIImage(named: "welcomeCode")
        let mapData = UIImagePNGRepresentation(map!)
        let receipt = UIImage(named: "receipt")
        let receiptData = UIImagePNGRepresentation(receipt!)
        let product = UIImage(named: "productScan")
        let productData = UIImagePNGRepresentation(product!)
        

        let scan = NSEntityDescription.insertNewObjectForEntityForName("Scans", inManagedObjectContext: context)
        scan.setValue(mapData, forKey: "image")
        scan.setValue("Macy's Welcome Code 3/3/2016", forKey: "title")

        let sca = NSEntityDescription.insertNewObjectForEntityForName("Scans", inManagedObjectContext: context)
        sca.setValue(receiptData, forKey: "image")
        sca.setValue("Macy's Receipt 3/3/2016", forKey: "title")
        
        let sc = NSEntityDescription.insertNewObjectForEntityForName("Scans", inManagedObjectContext: context)
        sc.setValue(productData, forKey: "image")
        sc.setValue("Ralph Lauren Buttondown", forKey: "title")

        do {
            try context.save()

        } catch {
            print("2: Error saving songs")
        }
        
        
        dict1Flag = false
        dict1Stop = false
        dict1Done = false
        
        dict2Flag = false
        dict2Stop = false
        dict2Done = false
        
        largeImageView.hidden = true
        
        currentCount = 0
        maxCount = 0
        numberOfImages = 0
        
        scanned = nil
        
        captureQR()
        
        imagePicker.delegate = self
        
    }
    
    func captureQR() {
        
        messageLabel.hidden = false
        
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
        // as the media type parameter.
        let captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        
        do {

            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            // Set the input device on the capture session.
            captureSession?.addInput(input)

            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
//            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0))
            
            // Detect all the supported bar code
            captureMetadataOutput.metadataObjectTypes = supportedBarCodes
            
            
            
            
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            view.layer.addSublayer(videoPreviewLayer!)
            
            // Start video capture
            captureSession?.startRunning()
            
            // Move the message label to the top view
            view.bringSubviewToFront(messageLabel)
            
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor.greenColor().CGColor
                qrCodeFrameView.layer.borderWidth = 2
                view.addSubview(qrCodeFrameView)
                view.bringSubviewToFront(qrCodeFrameView)
            }
            
            progress = KDCircularProgress(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
            progress.startAngle = -90
            progress.progressThickness = 0.2
            progress.trackThickness = 0.6
            progress.clockwise = true
            progress.gradientRotateSpeed = 2
            progress.roundedCorners = false
            progress.glowMode = .Forward
            progress.glowAmount = 0.9
            progress.trackColor = UIColor.clearColor()
            progress.setColors(UIColor.cyanColor() ,UIColor.whiteColor(), UIColor.magentaColor(), UIColor.whiteColor(), UIColor.orangeColor())
            progress.center = CGPoint(x: view.center.x, y: view.center.y)
            view.addSubview(progress)
            view.bringSubviewToFront(progress)
            
            progressLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
            progressLabel.text = ""
            progressLabel.font = UIFont.boldSystemFontOfSize(25)
            progressLabel.textColor = UIColor.whiteColor()
            progressLabel.layer.shadowColor = UIColor.blackColor().CGColor
            progressLabel.layer.shadowOffset = CGSize(width: 0, height: 0)
            progressLabel.layer.shadowOpacity = 0.8
            progressLabel.layer.shadowRadius = 5
            progressLabel.center = CGPoint(x: view.center.x, y: UIScreen.mainScreen().bounds.size.height - 100)
            progressLabel.textAlignment = .Center
            view.addSubview(progressLabel)
            view.bringSubviewToFront(progressLabel)
            
            numberLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 50))
            numberLabel.text = "Scan a QR Code!"
            numberLabel.font = UIFont.boldSystemFontOfSize(25)
            numberLabel.textColor = UIColor.whiteColor()
            numberLabel.layer.shadowColor = UIColor.blackColor().CGColor
            numberLabel.layer.shadowOffset = CGSize(width: 0, height: 0)
            numberLabel.layer.shadowOpacity = 0.8
            numberLabel.layer.shadowRadius = 0.5
            numberLabel.center = CGPoint(x: view.center.x, y: 80)
            numberLabel.textAlignment = .Center
            view.addSubview(numberLabel)
            view.bringSubviewToFront(numberLabel)
            
            let bookmarkButton = UIButton(frame: CGRect(x: UIScreen.mainScreen().bounds.size.width - 50, y: 25, width: 30, height: 30))
            bookmarkButton.setImage(UIImage(named: "hamburger"), forState: UIControlState.Normal)
            //            bookmarkButton.addTarget(self, action: "backToScan", forControlEvents: UIControlEvents.TouchUpInside)
            view.addSubview(bookmarkButton)
            
            let cameraButton = UIButton(frame: CGRect(x: 15, y: 25, width: 30, height: 30))
            cameraButton.setImage(UIImage(named: "camera"), forState: UIControlState.Normal)
            cameraButton.addTarget(self, action: "sendFilesChosen:", forControlEvents: UIControlEvents.TouchUpInside)
            view.addSubview(cameraButton)
            
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func image1Captured() {
        
        var image1Data:String = ""
        
        if dict1Done == true {
            
            print("image1Captured")
            
            let sortedData = self.dict1.sort() { $0.0 < $1.0 }
            
            for (key, value) in sortedData {
                
                if key == 0 {
                    
                    image1Data = value
                    
                } else {
                    
                    image1Data = "\(image1Data)\(value)"
                }
            }
            
            
        } else {
            
            print("not all images captured")
        }
        
        image1Data = image1Data.stringByReplacingOccurrencesOfString("data:image/x-icon;base64,", withString: "")
        
        switch image1Data.characters.count % 4 {
        case 0: break
        case 1: image1Data = "\(image1Data)==="
        case 2: image1Data = "\(image1Data)=="
        case 3: image1Data = "\(image1Data)="
        default: break
        }
        
        // SAVE IMAGE TO CORE DATA
        
        
        let imgData = NSData(base64EncodedString: image1Data, options: NSDataBase64DecodingOptions(rawValue: 0))
        let img = UIImage(data: imgData!)

        
        let imageView = UIImageView(frame: CGRect(x: 10, y: 0, width: 300, height: 300))
        imageView.contentMode = .ScaleAspectFit
        imageView.image = img
        imageView.center = CGPoint(x: view.center.x, y: view.center.y)
        imageView.alpha = 0
        view.addSubview(imageView)
        view.bringSubviewToFront(imageView)
        
        UIView.animateWithDuration(0.3) { () -> Void in
            imageView.alpha = 1
        }
        
        let imgPng = UIImagePNGRepresentation(img!)
        
        let scan = NSEntityDescription.insertNewObjectForEntityForName("Scans", inManagedObjectContext: context)
        scan.setValue(imgPng, forKey: "image")
        scan.setValue("New Scan 1", forKey: "title")
        
        do {
            try context.save()
            print("image1 saved")
        } catch {
            print("2: Error saving songs")
        }
        
        // SEGUE TO TABLEVIEW
        
    }
    
    
    
    
    func newAngle() -> Int {
        return Int(360 * (currentCount! / maxCount!))
    }
    
    
    var i = 0
    var string = String()
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRectZero
            messageLabel.text = "No barcode/QR code is detected"
            return
        }
        
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if string != metadataObj.stringValue {

            string = metadataObj.stringValue
        }
        
        
        // Here we use filter method to check if the type of metadataObj is supported
        // Instead of hardcoding the AVMetadataObjectTypeQRCode, we check if the type
        // can be found in the array of supported bar codes.
        if supportedBarCodes.contains(metadataObj.type) {
        
            
                    if metadataObj.type == AVMetadataObjectTypeQRCode {
//             If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObjectForMetadataObject(metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
        
            if metadataObj.stringValue != nil {
        

                messageLabel.text = metadataObj.stringValue
                
                
                let stringValue = metadataObj.stringValue
                
                let indices = stringValue.indicesOf("||")
        
//                 IMAGE ID
                    let index = stringValue.startIndex.advancedBy(indices[0])
                    let imageId = stringValue.substringToIndex(index)

//
//                    // IMAGE COUNT
//                    let a = stringValue.startIndex.advancedBy(indices[0] + 2)
//                    let b = stringValue.startIndex.advancedBy(indices[1])
//                    let imageCount = stringValue.substringWithRange(Range<String.Index>(start: a, end: b))
//                    
//                    
                // QR CODE INDEX
                let c = stringValue.startIndex.advancedBy(indices[1] + 2)
                let d = stringValue.startIndex.advancedBy(indices[2])
                let qrCodeIndex = stringValue.substringWithRange(Range<String.Index>(start: c, end: d))
        


                // QR CODE COUNT
                let e = stringValue.startIndex.advancedBy(indices[2] + 2)
                let f = stringValue.startIndex.advancedBy(indices[3])
                let qrCodeCount = stringValue.substringWithRange(Range<String.Index>(start: e, end: f))
//
                // DATA
                let g = stringValue.startIndex.advancedBy(indices[3] + 2)
                let data = stringValue.substringWithRange(Range<String.Index>(start: g, end: stringValue.endIndex))
//
                switch imageId {
                    
                case "0":
                    
                    if dict1Stop == false {
                        
                        dict1[Int(qrCodeIndex)!] = data
                        
                        if dict1Flag == false {
                            dict1Flag = true
                            
                            for var i = 0; i < Int(qrCodeCount)!; i++ {
                                dict1Count["\(i)"] = ""
                            }
                            
                            maxCount = maxCount + Int(qrCodeCount)!
                            progressLabel.text = "\(currentCount) / \(maxCount)"
                            
                            numberOfImages = numberOfImages + 1
                            if numberOfImages == 1 {
                                numberLabel.text = "Retrieving \(numberOfImages) image..."
                            } else if numberOfImages > 1 {
                                numberLabel.text = "Retrieving \(numberOfImages) images..."
                            }
                            
                        }
                        
                        let oldCount = dict1Count.count
                        
                        dict1Count.removeValueForKey(qrCodeIndex)
                        
                        let newCount = dict1Count.count
                        
                        if newCount < oldCount {
                            
                            currentCount = currentCount + 1
                            
                            progressLabel.text = "\(round(100 * (Double(currentCount) / Double(maxCount))))%"
                            
                            let newAngleValue: Double = (Double(360) * (Double(currentCount) / Double(maxCount)))
                            
                            progress.animateToAngle(Int(newAngleValue), duration: 0.5, completion: nil)
                            
                            if currentCount == maxCount {
                                
                                progressLabel.text = "Done!"
                                
                                numberLabel.text = ""
                                
                                self.parentPageViewController?.displayPageForIndex(1)
                                
                            }
                            
                        }
                        
                        
                        if dict1Count.count == 0 {
                            dict1Stop = true
                            dict1Done = true
                            self.image1Captured()
                            print("done with image 1")
                        }
                        
                    }
                    
                default: break
                    
                }
                
                // captureSession?.stopRunning()
                    
        
             
                        }
            }
            
        }
    }
    

    
    override func viewWillDisappear(animated: Bool) {
        
        // Deleting imageView once it's been displayed
        for subview in view.subviews {
            if subview.frame.size.height == 300 {
                subview.removeFromSuperview()
            }
        }
        
        progressLabel.text = ""
        
        currentCount = 0
        maxCount = 0
        
    }
}



extension String {
    
    /// Returns a new string made from the `String` by replacing all characters not in the unreserved
    /// character set (As defined by RFC3986) with percent encoded characters.
    
    func stringByAddingPercentEncodingForURLQueryParameter() -> String? {
        let allowedCharacters = NSCharacterSet.URLQueryParameterAllowedCharacterSet()
        return stringByAddingPercentEncodingWithAllowedCharacters(allowedCharacters)
    }
    
}

extension NSCharacterSet {
    
    /// Returns the character set for characters allowed in the individual parameters within a query URL component.
    ///
    /// The query component of a URL is the component immediately following a question mark (?).
    /// For example, in the URL `http://www.example.com/index.php?key1=value1#jumpLink`, the query
    /// component is `key1=value1`. The individual parameters of that query would be the key `key1`
    /// and its associated value `value1`.
    ///
    /// According to RFC 3986, the set of unreserved characters includes
    ///
    /// `ALPHA / DIGIT / "-" / "." / "_" / "~"`
    ///
    /// In section 3.4 of the RFC, it further recommends adding `/` and `?` to the list of unescaped characters
    /// for the sake of compatibility with some erroneous implementations, so this routine also allows those
    /// to pass unescaped.
    
    
    class func URLQueryParameterAllowedCharacterSet() -> Self {
        return self.init(charactersInString: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~/?")
    }
    
}



extension String {
    
    func split(len: Int) -> [String] {
        var currentIndex = 0
        var array = [String]()
        let length = self.characters.count
        while currentIndex < length {
            let startIndex = self.startIndex.advancedBy(currentIndex)
            let endIndex = startIndex.advancedBy(len, limit: self.endIndex)
            let substr = self.substringWithRange(Range(start: startIndex, end: endIndex))
            array.append(substr)
            currentIndex += len
        }
        return array
    }
    
    func indicesOf(searchTerm:String) -> [Int] {
        var indices = [Int]()
        var pos = self.startIndex
        while let range = self.rangeOfString(searchTerm, range: pos ..< self.endIndex) {
            indices.append(self.startIndex.distanceTo(range.startIndex))
            pos = range.startIndex.successor()
        }
        return indices;
    }
}








