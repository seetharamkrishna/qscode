//
//  TableViewController.swift
//  QSCode
//
//  Created by David Perkins on 2/27/16.
//  Copyright © 2016 David Perkins. All rights reserved.
//

import UIKit
import CoreData

class TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var appDel: AppDelegate!
    var context: NSManagedObjectContext!
    
    weak var parentPageViewController: PageViewController!
    
    @IBOutlet weak var tableView: UITableView!
    
    var scanTitles = [String]()
    var scanImages = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let navBar = UINavigationBar(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, 64))
        navBar.barTintColor = UIColor.whiteColor()
        navBar.translucent = false
        self.view.addSubview(navBar)
        
        let qrIcon = UIImage(named: "qrIcon")
        let qrButton = UIBarButtonItem(image: qrIcon, style: UIBarButtonItemStyle.Plain, target: self, action: "backToScan")
        let navItem = UINavigationItem(title: "Past Scans")
        
        
        navItem.leftBarButtonItem = qrButton
        navBar.items = [navItem]
        navBar.tintColor = UIColor.blackColor()
        
        appDel = UIApplication.sharedApplication().delegate as! AppDelegate
        context = appDel.managedObjectContext
        
        
    }
    
    func backToScan() {
        
        print("backToScan")
        self.parentPageViewController?.displayPageForIndex(0)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        let scanRequest = NSFetchRequest(entityName: "Scans")
        
        scanRequest.returnsObjectsAsFaults = false
        
        let scanResults = try? context.executeFetchRequest(scanRequest)
        
        // Adds all Core Data Facebook likes to array to compare
        if scanResults?.count > 0 {
            for result: AnyObject in scanResults! {
                if let title = result.valueForKey("title") as? String {
                    if !scanTitles.contains(title) {
                        scanTitles.append(title)
                        
                        if let imageData = result.valueForKey("image") as? NSData {
                            let image = UIImage(data: imageData)!
                            if !scanImages.contains(image) {
                                scanImages.append(image)
                            }
                        }
                    }
                    
                }
                
                
                
                tableView.reloadData()
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return scanTitles.count
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        self.performSegueWithIdentifier("scanToDetail", sender: indexPath)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)

//        cell.tag = indexPath.row
        
        cell.textLabel?.text = scanTitles[indexPath.row]

        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "scanToDetail" {
            
            let selectedIndexPath = sender as! NSIndexPath
            let index = selectedIndexPath.row
            
            let detailVC = segue.destinationViewController as! DetailViewController
            print(scanImages)
            detailVC.image = scanImages[index]
            
        }
    }
    
}
