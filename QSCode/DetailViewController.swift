//
//  DetailViewController.swift
//  QSCode
//
//  Created by David Perkins on 2/27/16.
//  Copyright © 2016 David Perkins. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var vWidth = self.view.frame.width
        var vHeight = self.view.frame.height
        
        scrollView.delegate = self
        scrollView.backgroundColor = UIColor(red: 90, green: 90, blue: 90, alpha: 0.90)
        scrollView.alwaysBounceVertical = false
        scrollView.alwaysBounceHorizontal = false
        scrollView.showsVerticalScrollIndicator = true
        scrollView.flashScrollIndicators()
        
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 10.0
        
        imageView!.layer.cornerRadius = 11.0
        imageView!.clipsToBounds = false
        
        
        imageView.image = image
        let navBar = UINavigationBar(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, 64))
        navBar.barTintColor = UIColor.whiteColor()
        navBar.translucent = false
        self.view.addSubview(navBar)
        
        let backIcon = UIImage(named: "back")
        let backButton = UIBarButtonItem(image: backIcon, style: UIBarButtonItemStyle.Plain, target: self, action: "back")
        
        let navItem = UINavigationItem(title: "View")
        navItem.leftBarButtonItem = backButton
        
        navBar.items = [navItem]
        navBar.tintColor = UIColor.blackColor()
    }
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func back() {
        
        self.navigationController?.popViewControllerAnimated(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
