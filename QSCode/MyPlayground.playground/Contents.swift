//: Playground - noun: a place where people can play

import UIKit


var str = "Hello, playground"


let base64String = "data:image/x-icon;base64,AAABAAEAEBAAAAAAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAQAQAAAAAAAAAAAAAAAAAAAAAAAA2Gin/Nhop/zYaKf82Gin/Nhop/zYaKf82Gin/Nhop/zYaKf82Gin/Nhop/zYaKf82Gin/Nhop/zYaKf82Gin/Nhop/zYaKf88IjD/XUlU/2RRW/9GLzv/kouQ/6Sdn/84Hiz/RCs4/0QrOP82Gyn/Nhop/0UtOv9FLDr/OR8t/zYaKf85Hyz/ubO2////////////7+/v//////+6tbb/RS47/+bl5v/Py87/OR4s/0gxPf/z8/P/vrq8/1dFTv82Gin/a1xk//////+4s7f/hnuB//b29v/+/v7/SjdC/0EoNv/5+Pn/39zg/zgdK/9oWGD//////5OIjf9ELTn/Nhop/6CYnP//////X0xW/zYaKf/Cvb///////0czPv9AJzX/9vX2/+Dc3/86Hy7/lIqP//////9jUlr/OB0s/zYaKf+qpKb//////2ZSXf82Gin/zcbJ//////9RPkf/QCc1//X19f/c2Nv/Pyc0/8nGyP/8/Pz/RzA8/zYaKf82Gin/q6Wn//////9kUVv/Nhop/83Gyf//////UD1G/0AnNf/19fX/1NDT/0w4RP/5+fn/zcnL/zkfLf82Gin/Nhop/6ulp///////ZFFb/zYaKf/Nxsn//////1A9Rv9AJzX/8fDx//n4+f/U0NP//////6GYnP82Gin/Nhop/zYaKf+rpaf//////2RRW/82Gin/zcbJ//////9QPUb/QCc0//Dv7///////+vr6///////39vf/RzM//zYbKf82Gin/q6Wn//////9kUVv/Nhop/83Gyf//////UD1G/0AnNP/19PT/4N3e/0s0QP+HfIL//////5uRlv9HLzv/Nhop/6ylp///////ZFFb/zYaKf/Nxsn//////1A9Rv9AJzT/9fX1/9zZ2v85Hiz/VD9K//////+7tLf/VkJM/zYaKf+po6X//////2RRW/82Gin/xb3C//////9OPEX/QCc0//b19f/c2tr/OR4s/1I9SP//////t7Gz/1M/SP82Gin/joWJ//////98b3b/Ri47/9XS1P//////STI+/0MpNv/y8fH/6+np/3FfaP+poab//////46Dif9CKjf/Nhop/0c0P//39vf///////r6+///////pp+k/zYaKf9GLjr/8O/v//////////////////Dv7/9AKjb/Nhop/zYaKf82Gin/UkBJ/7Osrv/DvsD/joSJ/zkhLv82Gin/PCIw/35wdv+GeH7/hHd8/39yef9HMT3/Nhop/zYaKf82Gin/Nhop/zYaKf82Gin/Nhop/zYaKf82Gin/Nhop/zYaKf82Gin/Nhop/zYaKf82Gin/Nhop/zYaKf82Gin/AAD//wAA//8AAP//AAD//wAA//8AAP//AAD//wAA//8AAP//AAD//wAA//8AAP//AAD//wAA//8AAP//AAD//w="


let decodedData = NSData(base64EncodedString: base64String, options:NSDataBase64DecodingOptions.IgnoreUnknownCharacters)

let image = UIImage(data: decodedData!)











extension String {
    
    /// Returns a new string made from the `String` by replacing all characters not in the unreserved
    /// character set (As defined by RFC3986) with percent encoded characters.
    
    func stringByAddingPercentEncodingForURLQueryParameter() -> String? {
        let allowedCharacters = NSCharacterSet.URLQueryParameterAllowedCharacterSet()
        return stringByAddingPercentEncodingWithAllowedCharacters(allowedCharacters)
    }
    
}


extension NSCharacterSet {
    
    /// Returns the character set for characters allowed in the individual parameters within a query URL component.
    ///
    /// The query component of a URL is the component immediately following a question mark (?).
    /// For example, in the URL `http://www.example.com/index.php?key1=value1#jumpLink`, the query
    /// component is `key1=value1`. The individual parameters of that query would be the key `key1`
    /// and its associated value `value1`.
    ///
    /// According to RFC 3986, the set of unreserved characters includes
    ///
    /// `ALPHA / DIGIT / "-" / "." / "_" / "~"`
    ///
    /// In section 3.4 of the RFC, it further recommends adding `/` and `?` to the list of unescaped characters
    /// for the sake of compatibility with some erroneous implementations, so this routine also allows those
    /// to pass unescaped.
    
    
    class func URLQueryParameterAllowedCharacterSet() -> Self {
        return self.init(charactersInString: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~/?")
    }
    
}



public func decodebase64(str : String) -> String{
    
    let decodedData     = NSData(base64EncodedString: str, options:NSDataBase64DecodingOptions(rawValue: 0))
    let decodedString   = String(data: decodedData!, encoding: NSUTF8StringEncoding)

    return decodedString!}