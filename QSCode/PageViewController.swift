//
//  PageViewController.swift
//  QSCode
//
//  Created by David Perkins on 2/27/16.
//  Copyright © 2016 David Perkins. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    private var pages: [UIViewController]!
    
    private var currentPageIndex: Int!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.dataSource = self
        self.delegate = self
        
        self.pages = [
            self.storyboard!.instantiateViewControllerWithIdentifier("ViewController"),
            self.storyboard!.instantiateViewControllerWithIdentifier("NavigationController") as! UINavigationController
        ]
        
        self.currentPageIndex = 0
        let startingViewController = self.pages[0] as UIViewController
        self.setViewControllers([startingViewController], direction: .Forward, animated: false, completion: nil)
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let index = (self.pages as NSArray).indexOfObject(viewController)
        self.currentPageIndex = index
        
        // if currently displaying last view controller, return nil to indicate that there is no next view controller
        return (self.currentPageIndex == self.pages.count - 1 ? nil : self.pages[self.currentPageIndex + 1])
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        let index = (self.pages as NSArray).indexOfObject(viewController)
        self.currentPageIndex = index
        
        // if currently displaying first view controller, return nil to indicate that there is no previous view controller
        return (index == 0 ? nil : self.pages[index - 1])
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func displayPageForIndex(index: Int, animated: Bool = true) {
        
        print("displayPageForIndex")
        
        assert(index >= 0 && index < self.pages.count, "Error: Attempting to display a page for an out of bounds index")
        
        // nop if index == self.currentPageIndex
        if self.currentPageIndex == index { return }
        
        if index < self.currentPageIndex {
            self.setViewControllers([self.pages[index]], direction: .Reverse, animated: true, completion: nil)
        } else if index > self.currentPageIndex {
            self.setViewControllers([self.pages[index]], direction: .Forward, animated: true, completion: nil)
        }
        
        self.currentPageIndex = index
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
